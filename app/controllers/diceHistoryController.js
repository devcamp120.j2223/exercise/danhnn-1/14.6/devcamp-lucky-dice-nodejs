// import model
const DiceHistoryModel = require('../models/diceHistoryModel')
const UserModel = require('../models/userModel')

const mongoose = require('mongoose');

const createDiceHistory = (request, response) => {
    //b1: thu thập dữ liệu
    let bodyRequest = request.body;
    console.log(bodyRequest)

    //b2: kiểm tra dữ liệu
    if (!bodyRequest.user) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "user is require"
        })
    }
    if (!(Number.isInteger(bodyRequest.dice) && bodyRequest.dice < 7 && bodyRequest.dice > 0)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "dice is number and  0 < dice < 7"
        })
    }
    // b3: Thao tác với cơ sở dữ liệu
    let createDiceHistory = {
        _id: mongoose.Types.ObjectId(),
        user: bodyRequest.user,
        dice: bodyRequest.dice
    }

    DiceHistoryModel.create(createDiceHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success Dice History created",
                data: data
            })
        }
    })
}

const getAllDiceHistory = (request, response) => {
    //b1: thu thập dữ liệu
    let user = request.query.user
    //b2: kiểm tra dữ liệu
    //b3: Thao tác với cơ sở dữ liệu

    let condition = {};

    if (user) {
        condition.user = user;
    }

    DiceHistoryModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get ALL Dice History Success",
                data: data
            })
        }
    })
}

const getDiceHistoryById = (request, response) => {
    //b1: thu thập dữ liệu
    let diceHistoryId = request.params.diceHistoryId
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "diceHistoryId is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    DiceHistoryModel.findById(diceHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get User By ID Success",
                data: data
            })
        }
    })
}

const updateDiceHistoryById = (request, response) => {
    //b1: thu thập dữ liệu
    let diceHistoryId = request.params.diceHistoryId
    let requestBody = request.body
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "diceHistoryId is not valid"
        })
    }

    //b3: Thao tác với cơ sở dữ liệu
    let updateDiceHistory = {
        user: requestBody.user,
        dice: requestBody.dice
    }
    DiceHistoryModel.findByIdAndUpdate(diceHistoryId, updateDiceHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Update Dice History By ID Success",
                data: data
            })
        }
    })
}

const deleteDiceHistoryById = (request, response) => {
    //b1: thu thập dữ liệu
    let diceHistoryId = request.params.diceHistoryId
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "diceHistoryId is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    DiceHistoryModel.findByIdAndDelete(diceHistoryId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Delete User By ID Success",
            })
        }
    })
}

const getDiceHistoryByUserName = (request, response) => {
    //b1: thu thập dữ liệu
    let username = request.query.username
    //b2: kiểm tra dữ liệu
    if (!username) {
        DiceHistoryModel.find((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get All Voucher History success",
                    data: data,
                })
            }
        })
    } else {
        UserModel.findOne({ username: username }, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                if (!data) {
                    response.status(400).json({
                        status: "Error 400: Username không tồn tại",
                        data: []
                    })
                } else {
                    UserModel.find({ username }, (error, data) => {
                        let condition = {};
                        condition.user = data[0]._id
                        DiceHistoryModel.find(condition, (error, data) => {
                            if (error) {
                                return response.status(500).json({
                                    status: "Error 500: Internal server error",
                                    message: error.message
                                })
                            } else {
                                return response.status(200).json({
                                    status: "Success: Get Voucher History By Username success",
                                    data: data,
                                })
                            }
                        })


                    })
                }
            }
        })
    }


    //b3: Thao tác với cơ sở dữ liệu

}



// export thành 1 module
module.exports = {
    createDiceHistory: createDiceHistory,
    getAllDiceHistory: getAllDiceHistory,
    getDiceHistoryById: getDiceHistoryById,
    updateDiceHistoryById: updateDiceHistoryById,
    deleteDiceHistoryById: deleteDiceHistoryById,
    getDiceHistoryByUserName: getDiceHistoryByUserName
}
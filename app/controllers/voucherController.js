// import model
const voucherModel = require('../models/voucherModel');

//import mongoose
const mongoose = require('mongoose');

const createVoucher = (request, response) => {
    //b1: thu thập dữ liệu
    let bodyRequest = request.body;
    //b2: kiểm tra dữ liệu
    if (!bodyRequest.code) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "code is require"
        })
    }

    if (!(Number.isInteger(bodyRequest.discount) && bodyRequest.discount >= 0 && bodyRequest.discount <= 100)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "voucher tối đa 100 thấp nhất 0"
        })
    }
    // b3: Thao tác với cơ sở dữ liệu
    let createvoucher = {
        _id: mongoose.Types.ObjectId(),
        code: bodyRequest.code,
        discount: bodyRequest.discount,
        note: bodyRequest.note
    }

    voucherModel.create(createvoucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success Voucher created",
                data: data
            })
        }
    })
}

const getAllVoucher = (request, response) => {
    //b1: thu thập dữ liệu
    //b2: kiểm tra dữ liệu
    //b3: Thao tác với cơ sở dữ liệu
    voucherModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get ALL Voucher Success",
                data: data
            })
        }
    })
}

const getVoucherById = (request, response) => {
    //b1: thu thập dữ liệu
    let voucherId = request.params.voucherId
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "voucherId is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    voucherModel.findById(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get Voucher By ID Success",
                data: data
            })
        }
    })
}

const updateVoucherById = (request, response) => {
    //b1: thu thập dữ liệu
    let voucherId = request.params.voucherId
    let requestBody = request.body
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "voucherId is not valid"
        })
    }

    //b3: Thao tác với cơ sở dữ liệu
    let updateVoucher = {
        code: requestBody.code,
        discount: requestBody.discount,
        note: requestBody.note
    }
    voucherModel.findByIdAndUpdate(voucherId, updateVoucher, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Update Voucher By ID Success",
                data: data
            })
        }
    })
}

const deleteVoucherById = (request, response) => {
    //b1: thu thập dữ liệu
    let voucherId = request.params.voucherId
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "voucherId is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    voucherModel.findByIdAndDelete(voucherId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Delete Voucher By ID Success",
            })
        }
    })
}

// export thành 1 module
module.exports = {
    createVoucher: createVoucher,
    getAllVoucher: getAllVoucher,
    getVoucherById: getVoucherById,
    updateVoucherById: updateVoucherById,
    deleteVoucherById: deleteVoucherById,
}
// import model
const VoucherHistoryModel = require('../models/voucherHistoryModel');

//import mongoose
const mongoose = require('mongoose');

const createVoucherHistory = (request, response) => {
    //b1: thu thập dữ liệu
    let bodyRequest = request.body;
    //b2: kiểm tra dữ liệu
    if (!bodyRequest.user) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "user is require"
        })
    }
    if (!bodyRequest.voucher) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "voucher is require"
        })
    }
    // b3: Thao tác với cơ sở dữ liệu
    let createVoucherHistory = {
        _id: mongoose.Types.ObjectId(),
        user: bodyRequest.user,
        voucher: bodyRequest.voucher
    }

    VoucherHistoryModel.create(createVoucherHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success Voucher History created",
                data: data
            })
        }
    })
}
const getAllVoucherHistory = (request, response) => {
    //b1: thu thập dữ liệu
    let user = request.query.user
    //b2: kiểm tra dữ liệu
    //b3: Thao tác với cơ sở dữ liệu
    let condition = {};
    
    if (user) {
        condition.user = user;
    }

    VoucherHistoryModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get ALL Voucher History Success",
                data: data
            })
        }
    })
}

const getVoucherHistoryById = (request, response) => {
    //b1: thu thập dữ liệu
    let historyId = request.params.historyId
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "historyId is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    VoucherHistoryModel.findById(historyId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get Voucher History By ID Success",
                data: data
            })
        }
    })
}
const updateVoucherHistoryById = (request, response) => {
    //b1: thu thập dữ liệu
    let historyId = request.params.historyId
    let requestBody = request.body
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "historyId is not valid"
        })
    }

    //b3: Thao tác với cơ sở dữ liệu
    let updateVoucherHistory = {
        user: requestBody.user,
        voucher: requestBody.voucher
    }
    VoucherHistoryModel.findByIdAndUpdate(historyId, updateVoucherHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Update Voucher History By ID Success",
                data: data
            })
        }
    })
}
const deleteVoucherHistoryById = (request, response) => {
    //b1: thu thập dữ liệu
    let historyId = request.params.historyId
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "historyId is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    VoucherHistoryModel.findByIdAndDelete(historyId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Delete Prize History By ID Success",
            })
        }
    })
}

// export thành 1 module
module.exports = {
    createVoucherHistory: createVoucherHistory,
    getAllVoucherHistory: getAllVoucherHistory,
    getVoucherHistoryById: getVoucherHistoryById,
    updateVoucherHistoryById: updateVoucherHistoryById,
    deleteVoucherHistoryById: deleteVoucherHistoryById,
}
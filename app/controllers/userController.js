// import model
const userModel = require('../models/userModel')

const mongoose = require('mongoose');

const createUser = (request, response) => {
    //b1: thu thập dữ liệu
    let bodyRequest = request.body;

    //b2: kiểm tra dữ liệu
    if (!bodyRequest.username) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "username is require"
        })
    }

    if (!bodyRequest.firstname) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "firstname is require"
        })
    }

    if (!bodyRequest.lastname) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "lastname is require"
        })
    }

    // b3: Thao tác với cơ sở dữ liệu
    let createUser = {
        _id: mongoose.Types.ObjectId(),
        username: bodyRequest.username,
        firstname: bodyRequest.firstname,
        lastname: bodyRequest.lastname
    }

    userModel.create(createUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success User created",
                data: data
            })
        }
    })
}

const getAllUser = (request, response) => {
    //b1: thu thập dữ liệu
    //b2: kiểm tra dữ liệu
    //b3: Thao tác với cơ sở dữ liệu
    userModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get ALL User Success",
                data: data
            })
        }
    })
}

const getUserById = (request, response) => {
    //b1: thu thập dữ liệu
    let userID = request.params.userID
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userID)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "userID is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    userModel.findById(userID, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get User By ID Success",
                data: data
            })
        }
    })
}

const updateUserById = (request, response) => {
    //b1: thu thập dữ liệu
    let userID = request.params.userID
    let requestBody = request.body
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userID)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "userID is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    let updateUser = {
        username: requestBody.username,
        firstname: requestBody.firstname,
        lastname: requestBody.lastname
    }
    userModel.findByIdAndUpdate(userID, updateUser, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Update User By ID Success",
                data: data
            })
        }
    })
}

const deleteUserById = (request, response) => {
    //b1: thu thập dữ liệu
    let userID = request.params.userID
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userID)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "userID is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    userModel.findByIdAndDelete(userID, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Delete User By ID Success",
            })
        }
    })
}

// export thành 1 modul
module.exports = {
    createUser: createUser,
    getAllUser: getAllUser,
    getUserById: getUserById,
    updateUserById: updateUserById,
    deleteUserById: deleteUserById
}
// import model
const prizeModel = require('../models/prizeModel');

//import mongoose
const mongoose = require('mongoose');


const createPrize = (request, response) => {
    //b1: thu thập dữ liệu
    let bodyRequest = request.body;
    //b2: kiểm tra dữ liệu
    if (!bodyRequest.name) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "name is require"
        })
    }
    // b3: Thao tác với cơ sở dữ liệu
    let createPrize = {
        _id: mongoose.Types.ObjectId(),
        name: bodyRequest.name,
        description: bodyRequest.description
    }

    prizeModel.create(createPrize, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success Prize created",
                data: data
            })
        }
    })
}

const getAllPrize = (request, response) => {
    //b1: thu thập dữ liệu
    //b2: kiểm tra dữ liệu
    //b3: Thao tác với cơ sở dữ liệu
    prizeModel.find((error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get ALL Prize Success",
                data: data
            })
        }
    })
}

const getPrizeById = (request, response) => {
    //b1: thu thập dữ liệu
    let prizeId = request.params.prizeId
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "prizeId is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    prizeModel.findById(prizeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get Prize By ID Success",
                data: data
            })
        }
    })
}

const updatePrizeById = (request, response) => {
    //b1: thu thập dữ liệu
    let prizeId = request.params.prizeId
    let requestBody = request.body
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "prizeId is not valid"
        })
    }

    //b3: Thao tác với cơ sở dữ liệu
    let updatePrize = {
        name: requestBody.name,
        description: requestBody.description
    }
    prizeModel.findByIdAndUpdate(prizeId, updatePrize, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Update Prize By ID Success",
                data: data
            })
        }
    })
}

const deletePrizeById = (request, response) => {
    //b1: thu thập dữ liệu
    let prizeId = request.params.prizeId
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(prizeId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "prizeId is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    prizeModel.findByIdAndDelete(prizeId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Delete Prize By ID Success",
            })
        }
    })
}

// export thành 1 module
module.exports = {
    createPrize: createPrize,
    getAllPrize: getAllPrize,
    getPrizeById: getPrizeById,
    updatePrizeById: updatePrizeById,
    deletePrizeById: deletePrizeById,
}
// import model
const PrizeHistoryModel = require('../models/prizeHistoryModel');
const UserModel = require('../models/userModel');
//import mongoose
const mongoose = require('mongoose');
const userModel = require('../models/userModel');

const createPrizeHistory = (request, response) => {
    //b1: thu thập dữ liệu
    let bodyRequest = request.body;
    //b2: kiểm tra dữ liệu
    if (!bodyRequest.user) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "user is require"
        })
    }
    if (!bodyRequest.prize) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "prize is require"
        })
    }
    // b3: Thao tác với cơ sở dữ liệu
    let createPrizeHistory = {
        _id: mongoose.Types.ObjectId(),
        user: bodyRequest.user,
        prize: bodyRequest.prize
    }

    PrizeHistoryModel.create(createPrizeHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(201).json({
                status: "Success Prize History created",
                data: data
            })
        }
    })
}
const getAllPrizeHistory = (request, response) => {
    //b1: thu thập dữ liệu
    let user = request.query.user
    //b2: kiểm tra dữ liệu
    //b3: Thao tác với cơ sở dữ liệu
    let condition = {};

    if (user) {
        condition.user = user;
    }

    PrizeHistoryModel.find(condition, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get ALL Prize History Success",
                data: data
            })
        }
    })
}
const getPrizeHistoryById = (request, response) => {
    //b1: thu thập dữ liệu
    let historyId = request.params.historyId
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "historyId is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    PrizeHistoryModel.findById(historyId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Get Prize History By ID Success",
                data: data
            })
        }
    })
}
const updatePrizeHistoryById = (request, response) => {
    //b1: thu thập dữ liệu
    let historyId = request.params.historyId
    let requestBody = request.body
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "historyId is not valid"
        })
    }

    //b3: Thao tác với cơ sở dữ liệu
    let updatePrizeHistory = {
        user: requestBody.user,
        prize: requestBody.prize
    }
    PrizeHistoryModel.findByIdAndUpdate(historyId, updatePrizeHistory, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(200).json({
                status: "Update Prize History By ID Success",
                data: data
            })
        }
    })
}

const deletePrizeHistoryById = (request, response) => {
    //b1: thu thập dữ liệu
    let historyId = request.params.historyId
    //b2: kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(historyId)) {
        return response.status(400).json({
            status: "ERROR 400: bad request",
            message: "historyId is not valid"
        })
    }
    //b3: Thao tác với cơ sở dữ liệu
    PrizeHistoryModel.findByIdAndDelete(historyId, (error, data) => {
        if (error) {
            return response.status(500).json({
                status: "Error 500: Internal sever error",
                message: error.message
            })
        } else {
            return response.status(204).json({
                status: "Delete Prize History By ID Success",
            })
        }
    })
}

const getPrizeHistoryByUserName = (request, response) => {
    //b1: thu thập dữ liệu
    let username = request.query.username
    //b2: kiểm tra dữ liệu
    if (!username) {
        PrizeHistoryModel.find((error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                return response.status(200).json({
                    status: "Success: Get All Prize History success",
                    data: data,
                })
            }
        })
    } else {
        UserModel.findOne({ username }, (error, data) => {
            if (error) {
                return response.status(500).json({
                    status: "Error 500: Internal server error",
                    message: error.message
                })
            } else {
                if (!data) {
                    response.status(400).json({
                        status: "Error 400: Username không tồn tại",
                        data: []
                    })
                } else {
                    UserModel.find({ username }, (error, data) => {
                        let condition = {};
                        condition.user = data[0]._id
                        PrizeHistoryModel.find(condition, (error, data) => {
                            if (error) {
                                return response.status(500).json({
                                    status: "Error 500: Internal server error",
                                    message: error.message
                                })
                            } else {
                                return response.status(200).json({
                                    status: "Success: Get Prize History By Username success",
                                    data: data,
                                })
                            }
                        })
                    })
                }
            }
        })
    }

    //b3: Thao tác với cơ sở dữ liệu

}

// export thành 1 module
module.exports = {
    createPrizeHistory: createPrizeHistory,
    getAllPrizeHistory: getAllPrizeHistory,
    getPrizeHistoryById: getPrizeHistoryById,
    updatePrizeHistoryById: updatePrizeHistoryById,
    deletePrizeHistoryById: deletePrizeHistoryById,
    getPrizeHistoryByUserName: getPrizeHistoryByUserName
}
//import express
const express = require('express');
// khởi tạo router
const router = express.Router();
// import controller
const { createDiceHistory, getAllDiceHistory, getDiceHistoryById, updateDiceHistoryById, deleteDiceHistoryById, getDiceHistoryByUserName } = require('../controllers/diceHistoryController');

router.post("/dice-histories", createDiceHistory)

router.get("/dice-histories", getAllDiceHistory)

router.get("/dice-histories/:diceHistoryId", getDiceHistoryById)

router.put("/dice-histories/:diceHistoryId", updateDiceHistoryById)

router.delete("/dice-histories/:diceHistoryId", deleteDiceHistoryById)

router.get("/devcamp-lucky-dice/dice-history", getDiceHistoryByUserName)



// export router thành 1 module
module.exports = router
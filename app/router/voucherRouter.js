//import express
const express = require('express');
// khởi tạo router
const router = express.Router();
// import controller
const { createVoucher, getAllVoucher, getVoucherById, updateVoucherById, deleteVoucherById } = require('../controllers/voucherController');

router.post("/vouchers", createVoucher)

router.get("/vouchers", getAllVoucher)

router.get("/vouchers/:voucherId", getVoucherById)

router.put("/vouchers/:voucherId", updateVoucherById)

router.delete("/vouchers/:voucherId", deleteVoucherById)


// export router thành 1 module
module.exports = router
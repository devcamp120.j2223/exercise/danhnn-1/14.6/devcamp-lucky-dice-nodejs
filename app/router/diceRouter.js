// Khai báo thư viện express
const express = require('express');

//Import  Controller
const { diceHandler } = require('../controllers/diceController');

// Tạo router
const diceRouter = express.Router();

// KHAI BÁO API

diceRouter.post('/devcamp-lucky-dice/dice', diceHandler)

// EXPORT ROUTER
module.exports = diceRouter;
//import express
const express = require('express');
// khởi tạo router
const router = express.Router();
// import controller
const { createPrize, getAllPrize, getPrizeById, updatePrizeById, deletePrizeById } = require('../controllers/prizeController');

router.post("/prizes", createPrize)

router.get("/prizes", getAllPrize)

router.get("/prizes/:prizeId", getPrizeById)

router.put("/prizes/:prizeId", updatePrizeById)

router.delete("/prizes/:prizeId", deletePrizeById)


// export router thành 1 module
module.exports = router
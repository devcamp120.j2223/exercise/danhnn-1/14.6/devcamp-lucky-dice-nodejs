//import express
const express = require('express');
// khởi tạo router
const router = express.Router();
// import controller
const {createUser, getAllUser, getUserById, updateUserById, deleteUserById} = require('../controllers/userController');

router.post("/user", createUser)

router.get("/user", getAllUser)

router.get("/user/:userID", getUserById)

router.put("/user/:userID", updateUserById)

router.delete("/user/:userID", deleteUserById)

// export router thành 1 module
module.exports = router
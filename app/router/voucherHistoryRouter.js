//import express
const express = require('express');
// khởi tạo router
const router = express.Router();
// import controller
const { createVoucherHistory, getAllVoucherHistory, getVoucherHistoryById, updateVoucherHistoryById, deleteVoucherHistoryById } = require('../controllers/voucherHistoryController');

router.post("/voucher-histories", createVoucherHistory)

router.get("/voucher-histories", getAllVoucherHistory)

router.get("/voucher-histories/:historyId", getVoucherHistoryById)

router.put("/voucher-histories/:historyId", updateVoucherHistoryById)

router.delete("/voucher-histories/:historyId", deleteVoucherHistoryById)


// export router thành 1 module
module.exports = router
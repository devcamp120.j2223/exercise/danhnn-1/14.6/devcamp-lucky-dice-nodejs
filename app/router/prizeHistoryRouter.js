//import express
const express = require('express');
// khởi tạo router
const router = express.Router();
// import controller
const { createPrizeHistory, getAllPrizeHistory, getPrizeHistoryById, updatePrizeHistoryById, deletePrizeHistoryById, getPrizeHistoryByUserName } = require('../controllers/prizeHistoryController');

router.post("/prizes-histories", createPrizeHistory)

router.get("/prizes-histories", getAllPrizeHistory)

router.get("/prize-histories/:historyId", getPrizeHistoryById)

router.put("/prize-histories/:historyId", updatePrizeHistoryById)

router.delete("/prize-histories/:historyId", deletePrizeHistoryById)

router.get("/devcamp-lucky-dice/prize-history", getPrizeHistoryByUserName)


// export router thành 1 module
module.exports = router